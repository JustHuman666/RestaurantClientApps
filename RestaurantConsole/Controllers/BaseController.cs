﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantConsole.Interfaces;

namespace RestaurantConsole.Controllers
{
    public class BaseController : IController
    {
        public Dictionary<int, Func<IController>> Controllers { get; set; }

        public BaseController()
        {
            Controllers = new Dictionary<int, Func<IController>>()
            {
                [1] = GoToIngredientController,
                [2] = GoToDishController,
                [3] = GoToOrderController,
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("\nChoose action what you want to do, write the number: " +
                "\n1 - work with ingredients" +
                "\n2 - work with dishes" +
                "\n3 - work with orders");
        }

        public IController GoToIngredientController()
        {
            return new IngredientController();
        }

        public IController GoToDishController()
        {
            return new DishController();
        }

        public IController GoToOrderController()
        {
            return new OrderController();
        }

    }
}
