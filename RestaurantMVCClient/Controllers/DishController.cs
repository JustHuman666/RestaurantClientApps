﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace RestaurantMVCClient.Controllers
{
    public class DishController : Controller
    {
       
        public ActionResult Index()
        {
            return View();
        }

        // GET all diches
        public ActionResult GetAll()
        {
           
            return View();
        }

        // GET: Ingredient/Add
        public ActionResult Add()
        {
            return View();
        }

        

        public ActionResult AddPrice(int id)
        {
            ViewBag.Id = id;
            return View();
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Id = id;
            return View();
        }
       
        public ActionResult Find()
        {
            return View();
        }
        
        public ActionResult EditPrice(int priceId, int dishId )
        {
            ViewBag.priceId = priceId;
            ViewBag.dishId = dishId;
            return View();
        }

     
        public ActionResult GetIngredients(int id)
        {
            ViewBag.id = id;
            return View();
            
        }
        public ActionResult AddIngredient(int id)
        {
            ViewBag.id = id;
            return View();
        }

    }
}