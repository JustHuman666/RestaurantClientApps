﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantConsole.EntetiesView
{
    public class DishView
    {
        /// <summary>
        /// An id of dish
        /// </summary>
        public int dishId { get; set; }

        /// <summary>
        /// The name of dish
        /// </summary>
        public string dishName { get; set; }
    }
}
