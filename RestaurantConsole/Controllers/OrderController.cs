﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using RestaurantConsole.EntetiesView;
using RestaurantConsole.Interfaces;

namespace RestaurantConsole.Controllers
{
    public class OrderController : IController
    {
        public Dictionary<int, Func<IController>> Controllers { get; set; }



        public OrderController()
        {
           
            Controllers = new Dictionary<int, Func<IController>>()
            {
                [1] = GetOrdersList,
                [2] = AddNewOrder,
                [3] = ChangeOrderInfo,
                [4] = FindOrdersByCustomer,
                [5] = DeleteOrder,
                [6] = AddDishToOrder,
                [7] = DeleteDishFromOrder,
                [8] = EditDish,
                [9] = GetListOfDishes,
                [10] = GoToMenu
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("\nChoose action what you want to do, write the number: " +
                "\n1 - get the list of all orders" +
                "\n2 - add new order in database" +
                "\n3 - change information about order" +
                "\n4 - find order for customer" +
                "\n5 - delete order from database" +
                "\n6 - add dish in order" +
                "\n7 - delete dish from order" +
                "\n8 - edit dish amount in order" +
                "\n9 - get the list of all dishes in one order" +
                "\n10 - return to the main menu\n");
        }

        public IController GetOrdersList()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.GetAsync
                    (@"https://localhost:5001/restaurant/order/getall").Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    List<OrderView> orders = JsonSerializer.Deserialize<List<OrderView>>(result);
                    foreach (var order in orders)
                    {
                        Console.WriteLine($"Order id: {order.orderId}; Customer name: {order.customerName}; \nDate and time of creating order: {order.orderDate}; Total price: {order.totalPrice}");
                    }
                    Console.WriteLine("");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController AddNewOrder()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the name of customer: ");
                var name = Console.ReadLine();
                var order = new OrderView() { customerName = name };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.PostAsync
                    (@"https://localhost:5001/restaurant/order/add", new StringContent(JsonSerializer.Serialize(order), Encoding.UTF8, "application/json")).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Console.WriteLine($"Order was created.");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController ChangeOrderInfo()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the id of order: ");
                var id = int.Parse(Console.ReadLine());
                Console.Write("Write the name of customer: ");
                var name = Console.ReadLine();
                var order = new OrderView() { customerName = name };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.PutAsync
                    (@"https://localhost:5001/restaurant/order/edit/" + id, new StringContent(JsonSerializer.Serialize(order), Encoding.UTF8, "application/json")).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Console.WriteLine($"Order was updated.");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController FindOrdersByCustomer()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the name of customer: ");
                var name = Console.ReadLine();
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.GetAsync
                    (@"https://localhost:5001/restaurant/order/find/" + name).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    List<OrderView> orders = JsonSerializer.Deserialize<List<OrderView>>(result);
                    int i = 1;
                    foreach (var order in orders)
                    {
                        Console.WriteLine($"{i++}. Order id: {order.orderId}; Customer name: {order.customerName}; \nDate and time of creating order: {order.orderDate}; Total price: {order.totalPrice}");
                    }
                    Console.WriteLine("");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController DeleteOrder()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the id of order: ");
                var id = int.Parse(Console.ReadLine());
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.DeleteAsync
                    (@"https://localhost:5001/restaurant/order/delete/" + id).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Console.WriteLine(result);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController AddDishToOrder()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the id of order: ");
                var orderId = int.Parse(Console.ReadLine());
                Console.Write("Write the id of portion and price of chosen dish: ");
                var portionId = int.Parse(Console.ReadLine());
                Console.Write("Write the amount of portions: ");
                var amount = int.Parse(Console.ReadLine());
                var orderDish = new OrderDishesView() { priceId = portionId, portionCount = amount };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.PostAsync
                    (@"https://localhost:5001/restaurant/order/adddish/" + orderId, new StringContent(JsonSerializer.Serialize(orderDish), Encoding.UTF8, "application/json")).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Console.WriteLine($"Such portion was added to chosen order.");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController DeleteDishFromOrder()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the id of dish in order: ");
                var orderDishId = int.Parse(Console.ReadLine());
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.DeleteAsync
                    (@"https://localhost:5001/restaurant/order/deletedish/" + orderDishId).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Console.WriteLine(result);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController GetListOfDishes()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the id of order: ");
                var id = int.Parse(Console.ReadLine());
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.GetAsync
                    (@"https://localhost:5001/restaurant/order/getdishes/" + id).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Dictionary<string, Dictionary<string, Dictionary<string, int>>> dishes = JsonSerializer.Deserialize<Dictionary<string, Dictionary<string, Dictionary<string, int>>>>(result);
                    int i = 1;
                    foreach (var order in dishes)
                    {
                        Console.WriteLine($"{i}. Dish in order id: {order.Key}. ");
                        int k = 1;
                        foreach (var dish in order.Value)
                        {
                            Console.WriteLine($"    {i}.{k++}. Dish name: {dish.Key}; Portion weight: {dish.Value["weight"]}; Price per portion: {dish.Value["price"]}; Amount of portions: {dish.Value["amount"]}; ");
                        }
                        i++;
                    }

                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController GoToMenu()
        {
            return new BaseController();
        }

        public IController EditDish()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the id of dish in order: ");
                var id = int.Parse(Console.ReadLine());
                Console.Write("Write new amount of portions: ");
                var amount = int.Parse(Console.ReadLine());
                var dish = new OrderDishesView() { portionCount = amount };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.PutAsync
                    (@"https://localhost:5001/restaurant/order/editdish/" + id, new StringContent(JsonSerializer.Serialize(dish), Encoding.UTF8, "application/json")).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Console.WriteLine("Info about this dish in this order was successfully edited.");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

    }
}
