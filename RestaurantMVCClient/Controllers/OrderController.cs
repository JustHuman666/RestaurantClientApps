﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace RestaurantMVCClient.Controllers
{
    public class OrderController : Controller
    {
        
        // GET: Dish
        public ActionResult Index()
        {
            return View();
        }

        // GET all diches
        public ActionResult GetAll()
        {
            return View();
        }

        // GET: Ingredient/Add
        public ActionResult Add()
        {
            return View();
        }
        public ActionResult AddDish(int id)
        {
            ViewBag.id = id;
            return View();

        }
        public ActionResult Edit(int id)
        {
            ViewBag.id = id;
            return View();
          
        }
        public ActionResult EditDish(int id)
        {
            ViewBag.id = id;
            return View();

        }
        public ActionResult GetDishes(int id)
        {
            ViewBag.id = id;
            return View();
        }

        public ActionResult Find()
        {
            return View();
        }
    }
}