﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantConsole.EntetiesView
{
    public class OrderView
    {
        /// <summary>
        /// The id of order
        /// </summary>
        public int orderId { get; set; }

        /// <summary>
        /// Total price of this order
        /// </summary>
        public decimal totalPrice { get; set; }

        /// <summary>
        /// Name of customer for this order
        /// </summary>
        public string customerName { get; set; }

        /// <summary>
        /// Date of creating of order
        /// </summary>
        public DateTime orderDate { get; set; }
    }
}
