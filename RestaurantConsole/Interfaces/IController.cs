﻿using System;
using System.Collections.Generic;

namespace RestaurantConsole.Interfaces
{
    public interface IController
    {
        Dictionary<int, Func<IController>> Controllers { get; set; }

        void ShowMenu();

    }
}
