﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantConsole.EntetiesView
{
    public class OrderDishesView
    {
        /// <summary>
        /// The id of dish in order
        /// </summary>
        public int orderDishesId { get; set; }

        /// <summary>
        /// The id of order which has this dish in
        /// </summary>
        public int orderId { get; set; }

        /// <summary>
        /// The id of price which has this dish
        /// </summary>
        public int priceId { get; set; }

        /// <summary>
        /// The amount of portion of this dish in this order
        /// </summary>
        public int portionCount { get; set; }
    }
}
