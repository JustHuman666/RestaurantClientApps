﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using RestaurantConsole.EntetiesView;
using RestaurantConsole.Interfaces;

namespace RestaurantConsole.Controllers
{
    public class DishController : IController
    {
        public Dictionary<int, Func<IController>> Controllers { get; set; }


        public DishController()
        {

            Controllers = new Dictionary<int, Func<IController>>()
            {
                [1] = GetDishesList,
                [2] = AddNewDish,
                [3] = ChangeDishInfo,
                [4] = FindDish,
                [5] = DeleteDish,
                [6] = AddIngredientForDish,
                [7] = DeleteIngredientFromDish,
                [8] = GetListOfIngredients,
                [9] = AddPrice,
                [10] = EditPrice,
                [11] = DeletePrice,
                [12] = GoToMenu
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("\nChoose action what you want to do, write the number: " +
                "\n1 - get the list of all dishes" +
                "\n2 - add new dish in database" +
                "\n3 - change information about dish" +
                "\n4 - find dish" +
                "\n5 - delete dish from database" +
                "\n6 - add ingredient for dish" +
                "\n7 - delete ingredient from dish" +
                "\n8 - get the list of all ingredients for one dish" +
                "\n9 - add new portion and price for the dish" +
                "\n10 - edit price for chosen portion of the dish" +
                "\n11 - delete chosen price for the dish" +
                "\n12 - return to the main menu\n");
        }

        public IController GetDishesList()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.GetAsync
                    (@"https://localhost:5001/restaurant/dish/getall").Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Dictionary<string, List<PriceListView>> menu = JsonSerializer.Deserialize<Dictionary<string, List<PriceListView>>>(result);
                    int i = 1;
                    foreach (var dish in menu)
                    {
                        var findResponse = client.GetAsync
                           (@"https://localhost:5001/restaurant/dish/getbyname/" + dish.Key).Result;
                        var foundDish = findResponse.Content.ReadAsStringAsync().Result;
                        var resDish = JsonSerializer.Deserialize<DishView>(foundDish);
                        var dishId = resDish.dishId;
                        Console.WriteLine($"{i++}. Dish id: {dishId}; Dish name: {dish.Key} \n");
                        if (dish.Value.Count != 0)
                        {
                            Console.WriteLine("Portions and prices: ");
                            foreach (var price in dish.Value)
                            {
                                Console.WriteLine($"Id of portion: {price.PriceId}; Portion weight: {price.Portion}; Price per portion: {price.PricePerPortion};");
                            }
                            Console.WriteLine("");
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController AddNewDish()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the name of dish: ");
                var name = Console.ReadLine();
                var dish = new DishView() { dishName = name };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.PostAsync
                    (@"https://localhost:5001/restaurant/dish/add", new StringContent(JsonSerializer.Serialize(dish), Encoding.UTF8, "application/json")).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    var dishRes = JsonSerializer.Deserialize<DishView>(result);
                    Console.WriteLine($"Dish was created: \nId: {dishRes.dishId}; Name: {dishRes.dishName}; \n");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController ChangeDishInfo()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the id of dish: ");
                var id = int.Parse(Console.ReadLine());
                Console.Write("Write new name of dish: ");
                var name = Console.ReadLine();
                var dish = new DishView() { dishName = name };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.PutAsync
                    (@"https://localhost:5001/restaurant/dish/edit/" + id, new StringContent(JsonSerializer.Serialize(dish), Encoding.UTF8, "application/json")).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    var dishRes = JsonSerializer.Deserialize<DishView>(result);
                    Console.WriteLine($"Dish was updated: \nId: {dishRes.dishId}; Name: {dishRes.dishName}; \n");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController FindDish()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the name of dish: ");
                var name = Console.ReadLine();
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.GetAsync
                    (@"https://localhost:5001/restaurant/dish/find/" + name).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Dictionary<string, List<PriceListView>> dishInfo = JsonSerializer.Deserialize<Dictionary<string, List<PriceListView>>>(result);
                    int i = 1;
                    foreach (var dish in dishInfo)
                    {
                        var findResponse = client.GetAsync
                           (@"https://localhost:5001/restaurant/dish/getbyname/" + dish.Key).Result;
                        var foundDish = findResponse.Content.ReadAsStringAsync().Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            var error = JsonSerializer.Deserialize<ErrorView>(result);
                            Console.WriteLine(error.message);
                            return this;
                        }
                        var resDish = JsonSerializer.Deserialize<DishView>(foundDish);
                        var dishId = resDish.dishId;
                        Console.WriteLine($"{i++}. Dish id: {dishId}; Dish name: {dish.Key} \n");
                        if (dish.Value.Count != 0)
                        {
                            Console.WriteLine("Portions and prices: ");
                            foreach (var price in dish.Value)
                            {
                                Console.WriteLine($"Id of portion: {price.PriceId}; Portion weight: {price.Portion}; Price per portion: {price.PricePerPortion};");
                            }
                            Console.WriteLine("");
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController DeleteDish()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the id of dish: ");
                var id = int.Parse(Console.ReadLine());
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.DeleteAsync
                    (@"https://localhost:5001/restaurant/dish/delete/" + id).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Console.WriteLine(result);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController AddIngredientForDish()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the id of dish: ");
                var dishId = int.Parse(Console.ReadLine());
                Console.Write("Write the id of ingredient: ");
                var ingrId = int.Parse(Console.ReadLine());
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.PostAsync
                    (@"https://localhost:5001/restaurant/dish/addingredient/" + dishId + "/" + ingrId, new StringContent("")).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Console.WriteLine($"Ingredient was added.");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController DeleteIngredientFromDish()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the id of dish: ");
                var dishId = int.Parse(Console.ReadLine());
                Console.Write("Write the id of ingredient: ");
                var ingrId = int.Parse(Console.ReadLine());
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.DeleteAsync
                    (@"https://localhost:5001/restaurant/dish/deleteingredient/" + dishId + "/" + ingrId).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Console.WriteLine(result);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController GetListOfIngredients()
        {
            try
            {
                Console.Write("Write the id of dish: ");
                var id = int.Parse(Console.ReadLine());
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.GetAsync
                    (@"https://localhost:5001/restaurant/dish/getingredients/" + id).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if(!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    List<IngredientView> ingredients = JsonSerializer.Deserialize<List<IngredientView>>(result);
                    Console.WriteLine("The list of all ingredients of chosen dish: ");
                    foreach (IngredientView ingr in ingredients)
                    {
                        Console.WriteLine($"Id: {ingr.ingredientId}; Name: {ingr.ingredientName}");
                    }
                    Console.WriteLine("");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }



        public IController AddPrice()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the id of dish: ");
                var id = int.Parse(Console.ReadLine());
                Console.Write("Write the portion weight: ");
                var weight = int.Parse(Console.ReadLine());
                Console.Write("Write the price per portion: ");
                var price = int.Parse(Console.ReadLine());
                var portion = new PriceListView() { DishId = id, Portion = weight, PricePerPortion = price };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.PostAsync
                    (@"https://localhost:5001/restaurant/dish/addprice/" + id, new StringContent(JsonSerializer.Serialize(portion), Encoding.UTF8, "application/json")).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Console.WriteLine("Price was successfully added.");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }
        public IController EditPrice()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the id of portion and price: ");
                var id = int.Parse(Console.ReadLine());
                Console.Write("Write new portion weight: ");
                var weight = int.Parse(Console.ReadLine());
                Console.Write("Write new price per portion: ");
                var price = int.Parse(Console.ReadLine());
                var portion = new PriceListView() { Portion = weight, PricePerPortion = price };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.PutAsync
                    (@"https://localhost:5001/restaurant/dish/editprice/" + id, new StringContent(JsonSerializer.Serialize(portion), Encoding.UTF8, "application/json")).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Console.WriteLine("Price was successfully edited.");
                }    
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }
        public IController DeletePrice()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the id of portion and price: ");
                var id = int.Parse(Console.ReadLine());
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.DeleteAsync
                    (@"https://localhost:5001/restaurant/dish/deleteprice/" + id).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Console.WriteLine("Price was successfully deleted.");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController GoToMenu()
        {
            return new BaseController();
        }
    }
}
