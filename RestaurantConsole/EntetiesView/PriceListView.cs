﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantConsole.EntetiesView
{
    public class PriceListView
    {
        /// <summary>
        /// The id of price for dish
        /// </summary>
        public int PriceId { get; set; }

        /// <summary>
        /// The id of dish in order
        /// </summary>
        public int DishId { get; set; }


        /// <summary>
        /// The weight of portion with this price
        /// </summary>
        public int Portion { get; set; }

        /// <summary>
        /// Price for one such portion of this dish
        /// </summary>
        public decimal PricePerPortion { get; set; }
    }
}
