﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using RestaurantConsole.EntetiesView;
using RestaurantConsole.Interfaces;
using System.Text.Json;
using System.Text;

namespace RestaurantConsole.Controllers
{
    public class IngredientController : IController
    {
        public Dictionary<int, Func<IController>> Controllers { get; set; }

        public IngredientController()
        {
            
            Controllers = new Dictionary<int, Func<IController>>()
            {
                [1] = GetIngredientsList,
                [2] = AddIngredient,
                [3] = ChangeIngredientInfo,
                [4] = FindIngredient,
                [5] = DeleteIngredient,
                [6] = GoToMenu
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("\nChoose action what you want to do, write the number: " +
                "\n1 - get the list of all ingredients" +
                "\n2 - add new ingredient in database" +
                "\n3 - change information about ingredient" +
                "\n4 - find ingredient" +
                "\n5 - delete ingredient from database" +
                "\n6 - return to the main menu\n");
        }

        public IController GetIngredientsList()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.GetAsync
                    (@"https://localhost:5001/restaurant/ingredient/getall").Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    List<IngredientView> ingredients = JsonSerializer.Deserialize<List<IngredientView>>(result);
                    foreach (IngredientView ingr in ingredients)
                    {
                        Console.WriteLine($"Id: {ingr.ingredientId}; Name: {ingr.ingredientName}");
                    }
                    Console.WriteLine("");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController AddIngredient()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the name of ingredient: ");
                var name = Console.ReadLine();
                var ingredient = new IngredientView() { ingredientName = name };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.PostAsync
                    (@"https://localhost:5001/restaurant/ingredient/add", new StringContent(JsonSerializer.Serialize(ingredient), Encoding.UTF8, "application/json")).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    var ingredientRes = JsonSerializer.Deserialize<IngredientView>(result);
                    Console.WriteLine($"Ingredient was created: \nId: {ingredientRes.ingredientId}; Name: {ingredientRes.ingredientName}; \n");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController ChangeIngredientInfo()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the id of ingredient: ");
                var id = int.Parse(Console.ReadLine());
                Console.Write("Write new name of ingredient: ");
                var name = Console.ReadLine();
                var ingredient = new IngredientView() { ingredientName = name };
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.PutAsync
                    (@"https://localhost:5001/restaurant/ingredient/edit/" + id, new StringContent(JsonSerializer.Serialize(ingredient), Encoding.UTF8, "application/json")).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    var ingredientRes = JsonSerializer.Deserialize<IngredientView>(result);
                    Console.WriteLine($"Ingredient was updated: \nId: {ingredientRes.ingredientId}; Name: {ingredientRes.ingredientName}; \n");
                }

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController FindIngredient()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the name of ingredient: ");
                var name = Console.ReadLine();
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.GetAsync
                    (@"https://localhost:5001/restaurant/ingredient/find/" + name).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    var ingredientRes = JsonSerializer.Deserialize<IngredientView>(result);
                    Console.WriteLine($"Ingredient was found: \nId: {ingredientRes.ingredientId}; Name: {ingredientRes.ingredientName}; \n");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController DeleteIngredient()
        {
            try
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };
                Console.Write("Write the id of ingredient: ");
                var id = int.Parse(Console.ReadLine());
                using (var client = new HttpClient(httpClientHandler))
                {
                    var response = client.DeleteAsync
                    (@"https://localhost:5001/restaurant/ingredient/delete/" + id).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        var error = JsonSerializer.Deserialize<ErrorView>(result);
                        Console.WriteLine(error.message);
                        return this;
                    }
                    Console.WriteLine(result);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine("Try again\n");
                return this;
            }
            return this;
        }

        public IController GoToMenu()
        {
            return new BaseController();
        }

    }
}
