﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantConsole.EntetiesView
{
    public class IngredientView
    {
        /// <summary>
        /// An id of ingredient
        /// </summary>
        public int ingredientId { get; set; }

        /// <summary>
        /// The name of ingredient
        /// </summary>
        public string ingredientName { get; set; }
    }
}
