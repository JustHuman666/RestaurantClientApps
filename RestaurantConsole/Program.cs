﻿using System;
using RestaurantConsole.Controllers;
using RestaurantConsole.Interfaces;

namespace RestaurantConsole
{
    class Program
    {
        static void Main()
        {
            
            IController controller = new BaseController();
            while (true)
            {
                int key;
                controller.ShowMenu();
                if (int.TryParse(Console.ReadLine(), out key) && controller.Controllers.ContainsKey(key))
                    controller = controller.Controllers[key].Invoke();
            }

        }
    }
}
